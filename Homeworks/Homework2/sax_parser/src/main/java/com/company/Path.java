package com.company;

public class Path {
    File file;
    Directory directory;

    public Path() {
    }

    public File getFile() {
        return file;
    }

    public Directory getDirectory() {
        return directory;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setDirectory(Directory directory) {
        this.directory = directory;
    }
}
