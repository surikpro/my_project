package com.company;

import java.util.List;

public class File {
    private String name;

    public File() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "/" + name;
    }
}
