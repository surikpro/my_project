package com.company;

import java.util.List;

public class Directory {
    private String name;

    public Directory() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "/" + name;
    }
}
