package com.company;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class PrintAllHandlerSax extends DefaultHandler {

    File file = new File();
    Directory directory = new Directory();
    Path path = new Path();
    private String currentTagName;
    String tagValue;

//    private static final String NAME_TAG = "name";
//    private static final String CHILDREN_TAG = "children";
    private static final String CHILD_TAG = "child";

    private boolean isFile;
    private boolean isDirectory;

    StringBuilder builder = new StringBuilder();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentTagName = qName;
        if (currentTagName.equals(CHILD_TAG)) {
            String attribute = attributes.getValue("is-file");
            if (attribute.equals("true")) {
                isFile = true;
            } else if (attribute.equals("false")) {
                isDirectory = true;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
//        System.out.println(new String(ch, start, length));
        if (currentTagName == null) {
            return;
        }
        tagValue = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (isFile) {
            file.setName(tagValue);
//            System.out.println(file);
        }
        if (isDirectory) {
            directory.setName(tagValue);
        }

        if (directory.getName() != null && !isFile) {
            builder.append(directory);
        }

        if (isFile) {
            builder.append(file);
            System.out.println(builder);
            builder.setLength(0);
        }

        currentTagName = null;
        isFile = false;
        isDirectory = false;
    }
}


