import com.company.Animal;
import com.company.Cat;
import com.company.Dog;

public class Main {
    public static void main(String[] args) {

        Cat cat1 = new Cat();
        cat1.setName("Murka");
        cat1.setSleeping(true);
        System.out.println(cat1.getName() + " says: " + cat1.voice());

        System.out.println("------------------------");

        Cat cat2 = new Cat();
        cat2.setName("Jessica");
        cat2.setEating(true);
        System.out.println(cat2.getName() + " says: " + cat2.wakeUp());

        System.out.println("------------------------");

        Dog dog = new Dog();
        dog.setName("Pirat");
        dog.setSleeping(false);
        System.out.println(dog.voice());
        System.out.println(dog.wakeUp());

    }
}
