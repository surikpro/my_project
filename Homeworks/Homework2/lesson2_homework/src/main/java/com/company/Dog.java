package com.company;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Dog extends Animal {
    private String name;
    private boolean isSleeping;

    @Override
    public String voice() {
        return "Gav-gav-gav - I am a barking dog!";
    }

    @Override
    public String wakeUp() {
        if (!isSleeping) {
            return "I am awake, I am living my life - I am the happiest dog in the world!!!";
        } else {
            return "I am sleeping";
        }
    }
}
