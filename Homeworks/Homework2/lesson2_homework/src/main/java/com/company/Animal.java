package com.company;

public abstract class Animal {

    public abstract String voice();

    public abstract String wakeUp();
}
