package com.company;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Cat extends Animal {

    private String name;
    private boolean isSleeping;
    private boolean isEating;

    @Override
    public String voice() {
        if (!isSleeping) {
            return "Meow-meow-meow - I am a lovely cat!";
        } else {
            return "I am sleeping...";
        }
    }

    @Override
    public String wakeUp() {
        if (isEating) {
            return "I am waking up! Wait for me!";
        } else {
            return "I am still sleeping...";
        }
    }
}
